# Nauru - jednostavna strateška igra

Igra je napravljana krajem 2018. kao završni projekt iz informatike.

Autori:
- Tin Plavec
- Andro Radičević

## Osnovni koncepti igre

Cilj igre je preživjeti što je dulje moguće. 

Na početku će biti postavljen jedan grad te dan određen broj resursa i ljudi. Resursi su drvo, hrana, kamen i novac. Resursi se troše tijekom vremena (drvo za grijanje, hrana za ljude…) i kada se nešto gradi (drvo, kamen i glina za građevine). Igra se bazira na potezima, a svaki potez označava jedan mjesec.

## Tijek igre

Igra počinje ožujka 1. godine. Godina ima četiri godišnja doba koja traju po tri mjeseca. Godišnja doba se razlikuju po potrošnji resursa, tijekom jeseni i zime se drvo jače troši za grijanje a tijekom zime hrana se uglavnom ne proizvodi. Događat će se i nasumični događaji koji će biti povoljni ili nepovoljni, primjerice bolesti, oluje, dulja zima ili bogata žetva. 

Grid igre je veličine 6x5 polja. Svako polje podržava jednu građevinu ovisno o svojim karakteristikama (šuma za drvosječe, planine za kamenolom…). Grad počinje s tri jedinice populacije (pop). S vremenom će se stvoriti još popova te će ih trebati zaposliti i zbrinuti.

## Početni parametri

- 3 popa
- 100 drva
- 100 hrane
- 50 novca
- 50 kamena

## Parametri igre

### Pop

Troši mjesečno:
- po proljeću: 3 hrane
- po ljetu: 3 hrane
- po jeseni: 4 hrane i 3 drva
- po zimi: 4 hrane i 5 drva

Daje mjesečno:
- 1 novac

Svakog proljeća se stvori novi po formuli `pop = pop + 1 + hrana // 100`.
Ako ostane bez hrane, jedan umire po mjesecu, a još jedan ako nema drva tijekom jeseni i zime.

### Farma

Može se izgraditi samo na ravnici.

Košta:
- 40 drva
- 10 kamena

Daje mjesečno:
- dok nije zima: 20 hrane

### Ribar

Može se izgraditi samo na moru.

Košta:
- 30 drva

Daje mjesečno:
- 10 hrane

### Drvosječa

Može se izgraditi samo u šumi.

Košta:
- 10 novaca

Troši mjesečno:
- 1 novac

Daje mjesečno:
- 10 drva

### Kamenolom

Može se izgraditi samo na planini.

Košta:
- 50 drva
- 30 kamena

Daje mjesečno:
- 10 kamena
