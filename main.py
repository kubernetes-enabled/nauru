from tkinter import *
from tkinter import messagebox
from random import *

resursi = 'resursi/'
dopusti_evente = True
bez_kraja = False

root = Tk()
root.title('Otok Nauru')
root.resizable(1,1)
root.geometry('+60+30')
gore = Label(root, height=7, text='Za dodatne informacije kliknite u bilo kojem trenutku ovaj dio prozora.')
mapa = Label(root)
resr = LabelFrame(root, text='Stanovništvo i resursi')
info = LabelFrame(root, text='Informacije')
grad = Label(root)
ctrl = Label(root)
doba = ['    zima','proljeće','    ljeto','  jesen']
extra = ['0','2','2','3']
mjes = 2
flag = mjes
kliknuto = 0
farma = ribar = drvosječa = kamenolom = 0
pop = 3
hrana = 100
drvo = 100
kamen = 50
novac = 50


def vrativrijeme(mjes):
    return str(doba[mjes%12//3])+' | '+str(mjes%12+1)+'. mjesec '+str(mjes//12+1)+'. godine '
def gradi(event,arg):
    global kliknuto, var, izg, L
    global farma, ribar, drvosječa, kamenolom
    global drvo, kamen, novac
    nastavi=0
    if(kliknuto==0):
        messagebox.showinfo('Informacije','Da biste izgradili građevinu, kliknite na nju, a potom na prikladno polje.')
    elif(kliknuto==1):
        if(izg[arg[0]][arg[1]]==1):
            messagebox.showwarning('Greška','Polje je već izgrađeno.')
        elif(drvo-40<0 or kamen-10<0):
            messagebox.showerror('Greška','Nemate dovoljno resursa!\nPotrebno je 40 drva i 10 kamena za izgradnju.')
        elif not(arg==(0,0) or arg==(0,1) or arg==(0,2) or arg==(0,3) or arg==(0,4) or arg==(1,0) or arg==(2,0) or arg==(3,0) or arg==(3,1) or arg==(4,0) or arg==(4,1) or arg==(4,3) or arg==(4,4)):
            nastavi=kliknuto
            messagebox.showwarning('Greška','Na tom polju nije moguće izgraditi farmu.\nOdaberite prikladno polje s ravnicom.')
        else:
            izg[arg[0]][arg[1]]=1
            farma+=1
            drvo-=40;kamen-=10
            dl['text']='Drvo | '+str(drvo);kl['text']='Kamen | '+str(kamen)
            L = []
            k=0
            for i in range(6):
                for j in range(5):
                    L.append(PhotoImage(file=resursi+str(i)+str(j)+'-'+extra[mjes%12//3]+'-'+str(izg[i][j])+'.png'))
                    var[i][j].configure(image=L[k])
                    k+=1
            h=root.winfo_height()
            w=root.winfo_width()
            root.geometry(str(w+1)+'x'+str(h))
            root.update()
            root.geometry(str(w)+'x'+str(h))
        
    elif(kliknuto==2):
        if(izg[arg[0]][arg[1]]==1):
            messagebox.showwarning('Greška','Polje je već izgrađeno.')
        elif(drvo-30<0):
            messagebox.showerror('Greška','Nemate dovoljno resursa!\nPotrebno je 30 drva za izgradnju.')
        elif not(arg[0]==5):
            nastavi=kliknuto
            messagebox.showwarning('Greška','Na tom polju nije moguće postaviti ribara.\nOdaberite prikladno polje s morem.')
        else:
            izg[arg[0]][arg[1]]=1
            ribar+=1
            drvo-=30
            dl['text']='Drvo | '+str(drvo)
            L = []
            k=0
            for i in range(6):
                for j in range(5):
                    L.append(PhotoImage(file=resursi+str(i)+str(j)+'-'+extra[mjes%12//3]+'-'+str(izg[i][j])+'.png'))
                    var[i][j].configure(image=L[k])
                    k+=1
            h=root.winfo_height()
            w=root.winfo_width()
            root.geometry(str(w+1)+'x'+str(h))
            root.update()
            root.geometry(str(w)+'x'+str(h))
    elif(kliknuto==3):
        if(izg[arg[0]][arg[1]]==1):
            messagebox.showwarning('Greška','Polje je već izgrađeno.')
        elif(novac-10<0):
            messagebox.showerror('Greška','Nemate dovoljno resursa!\nPotrebno je 10 novaca odmah.')
        elif not(arg==(1,1) or arg==(1,2) or arg==(1,3) or arg==(2,1) or arg==(3,2) or arg==(3,4)):
            nastavi=kliknuto
            messagebox.showwarning('Greška','Na tom polju nije moguće postaviti drvosječu.\nOdaberite prikladno polje sa šumom.')
        else:
            izg[arg[0]][arg[1]]=1
            drvosječa+=1
            novac-=10
            nl['text']='Novac | '+str(novac)
            L = []
            k=0
            for i in range(6):
                for j in range(5):
                    L.append(PhotoImage(file=resursi+str(i)+str(j)+'-'+extra[mjes%12//3]+'-'+str(izg[i][j])+'.png'))
                    var[i][j].configure(image=L[k])
                    k+=1
            h=root.winfo_height()
            w=root.winfo_width()
            root.geometry(str(w+1)+'x'+str(h))
            root.update()
            root.geometry(str(w)+'x'+str(h))
    elif(kliknuto==4):
        if(izg[arg[0]][arg[1]]==1):
            messagebox.showwarning('Greška','Polje je već izgrađeno.')
        elif(drvo-50<0 or kamen-30<0):
            messagebox.showerror('Greška','Nemate dovoljno resursa!\nPotrebno je 50 drva i 30 kamena za izgradnju.')
        elif not(arg==(1,4) or arg==(2,2) or arg==(2,3) or arg==(2,4) or arg==(3,3)):
            nastavi=kliknuto
            messagebox.showwarning('Greška','Na tom polju nije moguće izgraditi kamenolom.\nOdaberite prikladno polje s planinom.')
        else:
            izg[arg[0]][arg[1]]=1
            kamenolom+=1
            drvo-=50;kamen-=30
            dl['text']='Drvo | '+str(drvo);kl['text']='Kamen | '+str(kamen)
            L = []
            k=0
            for i in range(6):
                for j in range(5):
                    L.append(PhotoImage(file=resursi+str(i)+str(j)+'-'+extra[mjes%12//3]+'-'+str(izg[i][j])+'.png'))
                    var[i][j].configure(image=L[k])
                    k+=1
            h=root.winfo_height()
            w=root.winfo_width()
            root.geometry(str(w+1)+'x'+str(h))
            root.update()
            root.geometry(str(w)+'x'+str(h))
    kliknuto=nastavi
        
def gradi1(event):
    text['text']='FARMA\nZa izgradnju je potrebno 40 drva i 10 kamena\nProizvodi 20 hrane svaki mjesec koji nije zimski'
    info.pack()
    global kliknuto
    kliknuto=1
def gradi2(event):
    text['text']='RIBAR\nZa izgradnju barke je potrebno 30 drva\nProizvodi 10 hrane svaki mjesec'
    info.pack()
    global kliknuto
    kliknuto=2
def gradi3(event):
    text['text']='DRVOSJEČA\nKošta 10 novaca odmah te 1 novac mjesečno\nProizvodi 10 drva svaki mjesec'
    info.pack()
    global kliknuto
    kliknuto=3
def gradi4(event):
    text['text']='KAMENOLOM\nZa izgradnju je potrebno 50 drva i 30 kamena\nProizvodi 10 kamena svaki mjesec'
    info.pack()
    global kliknuto
    kliknuto=4
def klikpop(event):
    messagebox.showinfo('Informacije','Pop je jedinica populacije.\nUkoliko izgubite populaciju, igra je za vas završena.\nNa početku 5. mjeseca dobivate novu populaciju po formuli:\npop=pop+1+hrana//100\n\nPotrošnja po popu:\n   svaki proljetni i ljetni mjesec 3 hrane\n   svaki jesenski mjesec 4 hrane i 3 drva\n   svaki zimski mjesec 4 hrane i 5 drva\nDobivate 1 novac po popu svaki mjesec.\n\nIzgradite nove građevine kako biste osigurali dovoljno hrane za vašu populaciju. Za izgradnju pritisnite građevinu na dnu prozora, a potom željeno polje. Na jednom polju moguće je izgraditi samo jednu vrstu građevine!\nKad završite s potezom, pritisnite gumb Sljedeći mjesec ili razmaknicu na tipkovnici.')

def klik():
    global mjes
    global kliknuto, var, izg, L
    global flag
    global farma, ribar, drvosječa, kamenolom
    global pop, hrana, drvo, kamen, novac
    mjes+=1
    vrijeme['text']=vrativrijeme(mjes)
    if(mjes==4): gore['text']=(' '*130+'\n')*7
    kliknuto = 0

    L = []
    k=0
    for i in range(6):
        for j in range(5):
            L.append(PhotoImage(file=resursi+str(i)+str(j)+'-'+extra[mjes%12//3]+'-'+str(izg[i][j])+'.png'))
            var[i][j].configure(image=L[k])
            k+=1
    h=root.winfo_height()
    w=root.winfo_width()
    root.geometry(str(w+1)+'x'+str(h))
    root.update()
    root.geometry(str(w)+'x'+str(h))

    global photo1
    photo1 = PhotoImage(file=resursi+'Farma-'+extra[mjes%12//3]+'.png')
    gradnja1.config(image=photo1)
    global photo2
    photo2 = PhotoImage(file=resursi+'Ribar.png')
    gradnja2.config(image=photo2)
    global photo3
    photo3 = PhotoImage(file=resursi+'Drvosječa-'+extra[mjes%12//3]+'.png')
    gradnja3.config(image=photo3)
    global photo4
    photo4 = PhotoImage(file=resursi+'Kamenolom-'+extra[mjes%12//3]+'.png')
    gradnja4.config(image=photo4)

    if (mjes-flag>10 and dopusti_evente):
        if not(randint(0,3)):
            flag=mjes
            event=randint(0,4)
            if (event == 0):
                messagebox.showwarning('Događaj','Lopovi su provalili u vašu riznicu!\nSkladište novca smanjeno za 10.')
                novac-=10
            elif (event == 1):
                messagebox.showinfo('Događaj','U divljini je nađena stara škrinja s blagom!\nSkladište novca povećano za 15.')
                novac+=15
            elif (event == 2):
                messagebox.showinfo('Događaj','Dio vašeg drva je istrulio!\nSkladište drva smanjeno za 25.')
                drvo-=25
            elif (event == 3):
                messagebox.showinfo('Događaj','Ovoga mjeseca ljudi su jeli više nego uobičajeno!\nSkladište hrane smanjeno za 12.')
                hrana-=12
            elif (event == 4):
                event4=randint(0,1)
                if(ribar>0 and event4==0):
                    messagebox.showinfo('Događaj','Ribari su ostvarili velik ulov!\nSkladište hrane povećano za 20.')
                    hrana+=20
                if(farma>0 and event4==1):
                    messagebox.showinfo('Događaj','Velik urod na farmi!\nSkladište hrane povećano za 20.')
                    hrana+=20
            
    if (mjes%12==4): pop=pop+1+hrana//100
    novac=novac+pop-drvosječa
    kamen=kamen+kamenolom*10
    if   (mjes%12//3==0):hrana=hrana-pop*4+ribar*10;drvo=drvo-pop*5+drvosječa*10
    elif (mjes%12//3==1):hrana=hrana-pop*3+farma*20+ribar*10;drvo=drvo+drvosječa*10
    elif (mjes%12//3==2):hrana=hrana-pop*3+farma*20+ribar*10;drvo=drvo+drvosječa*10
    elif (mjes%12//3==3):hrana=hrana-pop*4+farma*20+ribar*10;drvo=drvo-pop*3+drvosječa*10
    if(hrana<0):
        hrana = 0
        pop-=1
    if(drvo<0):
        drvo = 0
        if (mjes%12//3==0 or mjes%12//3==3): pop-=1
    if(kamen<0):
        kamen = 0
    if(novac<0):
        novac = 0
        
    pl['text']='Pop  | '+str(pop)
    hl['text']='Hrana | '+str(hrana)
    dl['text']='Drvo | '+str(drvo)
    kl['text']='Kamen | '+str(kamen)
    nl['text']='Novac | '+str(novac)
    if (hrana==0 and drvo==0): s='Oprez, nemate hrane ni drva!\nGubite 1 pop svaki mjesec,a tijekom jeseni i zime gubite 2 popa'
    elif (hrana==0): s='Oprez, nemate hrane!\nGubite 1 pop svaki mjesec'
    elif (drvo==0): s='Oprez, nemate drva!\nGubite 1 pop svaki mjesec tijekom jeseni i zime'
    elif (mjes%12==2): s='Sljedeći mjesec dolazi proljeće\nViše se drva ne troše na grijanje'
    elif (mjes%12==4): s='Dobili ste novu populaciju'
    elif (mjes%12==5): s='Sljedeći mjesec dolazi ljeto'
    elif (mjes%12==8): s='Sljedeći mjesec dolazi jesen\nSvaki pop trošit će 3 drva na grijanje'
    elif (mjes%12==11): s='Sljedeći mjesec dolazi zima\nSvaki pop trošit će 5 drva na grijanje'
    else: s=''
    if(s==''): info.pack_forget()
    else:
        text['text']=s
        info.pack()
    if (pop==0 and not(bez_kraja)):
        messagebox.showwarning('Greška','Vaše je naselje ostalo bez populacije! Nauru je propao.\nMeđutim, postoji mogućnost od 10% da se u vaše naselje useli nova populacija.\n\nKliknite OK za nastavak.')
        messagebox.showerror('Kraj igre','U vaše se naselje uselila nova populacija. Nauru je opet živ!\nMeđutim, nova populacija odbija vašu vlast.\n\nZa vas je igra završena.\nMjeseci na vlasti: '+str(mjes-2))
        h=root.winfo_height()
        w=root.winfo_width()
        for i in range(h):
            root.geometry(str(w)+'x'+str(h-i))
            root.update()
        for i in range(w):
            root.geometry(str(w-i)+'x'+'0')
            root.update()
        root.destroy()
var = []
izg = []
L = []
k=0
for i in range(6):
    var.append([])
    izg.append([])
    for j in range(5):
        var[i].append([])
        izg[i].append(0)
        L.append(PhotoImage(file=resursi+str(i)+str(j)+'-'+extra[mjes%12//3]+'-'+str(izg[i][j])+'.png'))
        var[i][j] = Label(mapa,image=L[k])
        var[i][j].grid(row=i,column=j)
        var[i][j].bind('<ButtonRelease>',lambda event,arg=(i,j): gradi(event, arg))
        k+=1

gumb = Button(ctrl, command=klik, text='Sljedeći mjesec')
vrijeme = Label(ctrl, height=2, text=vrativrijeme(mjes))

pl = Label(resr, text='Pop  | '+str(pop))
hl = Label(resr, text='Hrana | '+str(hrana))
dl = Label(resr, text='Drvo | '+str(drvo))
kl = Label(resr, text='Kamen | '+str(kamen))
nl = Label(resr, text='Novac | '+str(novac))
pl.bind('<ButtonRelease>',klikpop)
gore.bind('<ButtonRelease>',klikpop)

text = Label(info, text='Sljedeći mjesec dolazi proljeće')

photo1 = PhotoImage(file=resursi+'Farma-'+extra[mjes%12//3]+'.png')
gradnja1 = Label(grad, image=photo1)
gradnja1.bind('<ButtonRelease>',gradi1)
photo2 = PhotoImage(file=resursi+'Ribar.png')
gradnja2 = Label(grad, image=photo2)
gradnja2.bind('<ButtonRelease>',gradi2)
photo3 = PhotoImage(file=resursi+'Drvosječa-'+extra[mjes%12//3]+'.png')
gradnja3 = Label(grad, image=photo3)
gradnja3.bind('<ButtonRelease>',gradi3)
photo4 = PhotoImage(file=resursi+'Kamenolom-'+extra[mjes%12//3]+'.png')
gradnja4 = Label(grad, image=photo4)
gradnja4.bind('<ButtonRelease>',gradi4)

mapa.pack(side='left')
gore.pack()
text.pack(padx=5)
info.pack()
gumb.pack(side='right')
vrijeme.pack()
ctrl.pack(side='bottom')
gradnja4.pack(side='right')
gradnja3.pack(side='right')
gradnja2.pack(side='right')
gradnja1.pack(side='right')
grad.pack(side='bottom')
pl.pack()
hl.pack()
dl.pack()
kl.pack()
nl.pack()
resr.pack(side='bottom')
def tipka(event):
    if(event.char==' '):
        gumb.invoke()
root.bind('<KeyPress>',tipka)
messagebox.showinfo('Uvodne informacije','Dobrodošli na Otok Nauru!\nCilj vam je držati vaše naselje što dulje na životu.\nVitalna jedinica naselja je pop, odnosno jedinica populacije. Populacija troši hranu i drvo, a daje vam novac od poreza.\nIzgradite nove građevine kako biste osigurali dovoljno hrane za vašu populaciju. Za izgradnju pritisnite građevinu na dnu prozora, a potom željeno polje. Na jednom polju moguće je izgraditi samo jednu vrstu građevine!\nKad završite s potezom, pritisnite gumb Sljedeći mjesec.\n\nSretno!')
root.focus_force()
root.mainloop()
